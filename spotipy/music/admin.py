import time

from django.contrib import admin

# Register your models here.
from music.models import Genre, Artist, Song


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    fields = ('name',)
    search_fields = ('name', )

    list_display = ('name',)


@admin.register(Artist)
class ArtistAdmin(admin.ModelAdmin):
    fields = ('name', 'genre',)
    search_fields = ('name',)

    list_display = ('name', 'genre',)


@admin.register(Song)
class SongAdmin(admin.ModelAdmin):
    fields = ('name', 'artist', 'duration', 'status', 'song_file')
    search_fields = ('name',)

    list_display = ('name', 'artist', 'duration_format', 'status', 'song_file')
    list_filter = ('status', )
    
    def duration_format(self, obj):
        return time.strftime('%M:%S', time.gmtime(obj.duration))
    duration_format.short_description = 'Duration'
    duration_format.admin_order_field = 'duration'
