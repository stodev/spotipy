from django.db import models
from django.db.models import ForeignKey
from django.utils.translation import gettext_lazy as _
from django.http import HttpResponseRedirect
from django.shortcuts import render


# Create your models here.
class Genre(models.Model):
    name = models.CharField(max_length=255, default='Genre Unknown')

    def __str__(self):
        return self.name


class Artist(models.Model):
    name = models.CharField(max_length=255, default='Artist Unknown')
    genre: ForeignKey = models.ForeignKey(Genre, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Song(models.Model):
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, default='No Title')
    duration = models.IntegerField(default=0, help_text="duration in seconds")  # en seconds

    class Status(models.TextChoices):
        DRAFT = 'DRAFT', _('Draft')
        PUBLISHED = 'PUBLISHED', _('Published')
        PRIVATE = 'PRIVATE', _('Private')

    status = models.CharField(max_length=10, choices=Status.choices, default=Status.DRAFT)

    song_file = models.FileField(upload_to='uploads/%Y/%m/%d', blank=True)

    def __str__(self):
        return self.name  # , self.artist.name, self.status, self.duration
